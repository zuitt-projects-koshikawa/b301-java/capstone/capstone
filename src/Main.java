import java.util.Scanner;

//        Scanner name = new Scanner();
public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573","Caloocan City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty.");
        } else {
            // for each contact in the phonebook, display the details
            for (Contact contact : phonebook.getContacts()) {
                String notifMsg = String.format("\nName: %s\nContact Number: %s\nAddress: %s", contact.getName(), contact.getContactNumber(), contact.getAddress());
                System.out.println(notifMsg);
            }
        }

        System.out.println("\n");

        String output1 = String.format("%s has the following registered number: \n%s", contact1.getName(), contact1.getContactNumber());
        String output2 = String.format("%s has the following registered address: \n%s", contact1.getName(), contact1.getAddress());

        String output3 = String.format("%s has the following registered number: \n%s", contact2.getName(), contact2.getContactNumber());
        String output4 = String.format("%s has the following registered address: \n%s", contact2.getName(), contact2.getAddress());

        System.out.println(contact1.getName());
        System.out.println("-------------------");

        System.out.println(output1);
        System.out.println(output2);

        System.out.print("\n");
        System.out.println(contact2.getName());
        System.out.println("-------------------");

        System.out.println(output3);
        System.out.println(output4);

    }
}